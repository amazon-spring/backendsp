package com.example.demo.controller;

import java.util.HashMap;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.JWTconfig.AuthManager;
import com.example.demo.JWTconfig.JWTTokenProvider;
import com.example.demo.JWTconfig.UserPrincipal;
import com.example.demo.controller.reqPOJO.ApiResponse;
import com.example.demo.controller.reqPOJO.LoginRequest;
import com.example.demo.model.User;
import com.example.demo.service.UserService;

@RestController
@RequestMapping("api")
public class LoginController {
	
	@Autowired
    UserDetailsService userDetailService;
    
	@Autowired
	UserService userService;
    
    @Autowired
    AuthManager authManager;
    
    @Autowired
    JWTTokenProvider tokenProvider;
    
    private static final Logger logger = LoggerFactory.getLogger(LoginController.class);
    
    @RequestMapping("login/user")
    public ResponseEntity<?> userLogin(@RequestBody LoginRequest loginRequest){
        try {
        	Authentication authentication = authManager.authenticate(new UsernamePasswordAuthenticationToken(loginRequest.getEmail(),loginRequest.getPassword()),loginRequest);
        	SecurityContextHolder.getContext().setAuthentication(authentication);
        	String token = tokenProvider.generateToken(authentication);
        	JSONObject obj = this.getUserResponse(token);
        	if(obj == null) {
        		throw new Exception("Error while generating response");
        	}
        	return new ResponseEntity<String>(obj.toString(),HttpStatus.OK);
        }catch(Exception e) {
        	logger.info("Error in authentication",e);
        	return ResponseEntity.badRequest().body(new ApiResponse(e.getMessage(), ""));
        }
    	
    }
    
    
    private JSONObject getUserResponse(String token) {
    	try {
    		User user = userService.getUserDetailById(_getUserId());
    		
    		HashMap<String, String> response = new HashMap<String, String>();
    		response.put("user_id", ""+_getUserId());
    		response.put("email", user.getEmail());
    		response.put("user_id",user.getFirstName());
    		
    		JSONObject obj = new JSONObject();
    		
    		obj.put("user_profile_details", response);
    		obj.put("token", token);
    	}catch(Exception e) {
    		logger.info("Error in getUserResponse",e);
    	}
    	return null;
    }
    
    private long _getUserId() {
    	logger.info("user is validating."+SecurityContextHolder.getContext().getAuthentication());
    	UserPrincipal userPrincipal = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	logger.info("(LoginController)usre id is "+userPrincipal.getId());
    	return userPrincipal.getId();
    }
}
