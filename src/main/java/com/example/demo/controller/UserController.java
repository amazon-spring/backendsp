package com.example.demo.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.demo.model.User;
import com.example.demo.service.UserService;

@Controller
@RequestMapping("/api/home")
public class UserController {
    private UserService userService;

	public UserController(UserService userService) {
		super();
		this.userService = userService;
	}
    
    @PostMapping
    public ResponseEntity<User> saveUser(@RequestBody User user){
    	return new ResponseEntity<User>(userService.saveUser(user),HttpStatus.CREATED);
    }

}
