package com.example.demo.serviceImpl;

import org.springframework.stereotype.Service;

import com.example.demo.model.User;
import com.example.demo.repository.UserRepository;
import com.example.demo.service.UserService;

@Service
public class UserServiceImpl implements UserService{
    private UserRepository userRepository;

	public UserServiceImpl(UserRepository userRepository) {
		super();
		this.userRepository = userRepository;
	}

	@Override
	public User saveUser(User user) {
		// TODO Auto-generated method stub
		return userRepository.save(user);
	}

	@Override
	public User findByEmail(String email) throws Exception {
		// TODO Auto-generated method stub
		return userRepository.findByEmail(email).orElseThrow(()-> new Exception("User not found.."));
	}

	@Override
	public User getUserDetailById(long id) throws Exception {
		
		return userRepository.findById(id).orElseThrow(()->new Exception("User Not found.."));
	}
    
    
}
